## Goal of the project

It is supposed to be part of a bigger project to automate search of movies on torrent trackers.

### Requirements

* `python`
* `Flask`
* `PostgreSQL`

Install requirements:
```shell
pip install -r requirements.txt
```

### Setting PostgreSQL

```shell
brew install postgresql
```

Create database and table:
```shell
# start postgres
postgres -D /usr/local/var/postgres
createdb filmapi
psql -d filmapi
CREATE TABLE films (id SERIAL PRIMARY KEY, film VARCHAR(100), year INT);
CREATE UNIQUE INDEX index_films ON films (film, year);
\l
```

TODO: automate db creation

You will get list of your databases, remember owner name. Quit psql

```
\q
```

Run the server:

```
python hello.py
```

Open your browser, http://127.0.0.1:5000, there will be:

```
It's alive
```

## Usage

Now you can make curl requests in your favorite shell

1.Add new film in database:

```
USER=<db_owner_username> curl -XPOST http://127.0.0.1:5000/film -d '{"title": "Shrek", "year": "2003"}'
USER=<db_owner_username> curl -XPOST http://127.0.0.1:5000/film -d '{"title": "Terminator", "year": "1991"}'
```

Response:

```
200 OK
```

If film already in the base:

```
409 Resource Conflict
```

2. Get film with id 1 from database:

```
USER=<db_owner_username> curl -XGET http://127.0.0.1:5000/film/1
```

Response:

```
{'year': 2003, 'id': 1, 'title': 'Shrek'}
200 OK
```

3. Get film with id 100:

```
USER=<db_owner_username> curl -XGET http://127.0.0.1:5000/film/100
```

Response:

```
404 NOT FOUND
```

4. Get films list from database:

```
USER=<db_owner_username> curl -XGET http://127.0.0.1:5000/film
```

Response:

```
{id: 1, film: Shrek, year: 2003}
{id: 2, film: Terminator, year: 1991}
200 OK
```









