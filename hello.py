from flask import Flask, request
import os
import json
import psycopg2

APP = Flask(__name__)
USER = os.environ.get('USER')


@APP.route('/', )
def post_film():
    return 'It\'s alive!'


@APP.route('/film', methods=['POST'])
def put_film():
    conn = psycopg2.connect('dbname=filmapi user=%s' % USER)
    cur = conn.cursor()
    request_data = json.loads(request.get_data(as_text=True))
    film_title = request_data['title']
    film_year = request_data['year']
    try:
        cur.execute('INSERT INTO films (film, year) VALUES (%s, %s);',
                    (film_title, film_year))
        conn.commit()
        return '200 ok'
    except psycopg2.IntegrityError:
        return '409 Resource Conflict'


@APP.route('/film/<film_id>', methods=['GET'])
def get_film(film_id):
    conn = psycopg2.connect('dbname=filmapi user=%s' % USER)
    cur = conn.cursor()

    cur.execute('SELECT id, film, year FROM films WHERE ID = %s;', (film_id,))
    id_film_year = cur.fetchone()
    if not id_film_year:
        return '404 NOT FOUND'
    else:
        films = {'id': id_film_year[0],
                 'title': id_film_year[1],
                 'year': id_film_year[2]}
        return '{}\n200 OK'.format(films)


@APP.route('/film', methods=['GET'])
def get_all_films():
    conn = psycopg2.connect('dbname=filmapi user=%s' % USER)
    cur = conn.cursor()
    cur.execute('SELECT id, film, year FROM films;')
    films_table = cur.fetchall()
    films_list = ''
    for id_film in films_table:
        id = id_film[0]
        film = id_film[1]
        year = id_film[2]
        films_list += '{id: %s, film: %s, year: %s}\n' % (id, film, year)
    return '{}\n 200 OK'.format(films_list)


if __name__ == "__main__":
    APP.run(debug=False)
